from django.conf.urls import patterns, url

from winamp import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^/json$', views.as_json, name='json'),
)