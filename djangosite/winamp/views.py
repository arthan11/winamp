from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
from winamp.winamp_api import Winamp
from winamp.models import radio
import win32api, win32gui, win32con
#from time import sleep
from subprocess import Popen, PIPE
import json

def open():
    #cmd = r'"C:\Program Files (x86)\Winamp\winamp.exe"' # x64
    cmd = r'"C:\Program Files\Winamp\winamp.exe"'
    Popen(cmd , shell=True, stdout=PIPE, stderr=PIPE, stdin=PIPE)
    #while win32gui.FindWindow('Winamp v1.x', None) == 0:
    #    pass

def is_open():
    try:
        return win32gui.FindWindow('Winamp v1.x', None) > 0
    except:
        return False

def close():
    hwnd = win32gui.FindWindow('Winamp v1.x', None)
    win32gui.SendMessage(hwnd, win32con.WM_CLOSE, 0, 0)


def as_json(request):
    w = Winamp()
    title = w.getCurrentPlayingTitle()
    pos = w.getPlayingTrackPosition() / 1000
    pos_m, pos_s = divmod(pos, 60)
    cur_time = '%0*d:%0*d' % (2, pos_m, 2, pos_s)
    len = w.getPlayingTrackLength()
    vol = int(w.getVolume() / 255.0 * 100)

    j = json.dumps({'title': title, 'volume': str(vol), 'cur_time': cur_time, 'volume': vol})
    return HttpResponse(j)

def index(request):
    try:
        w = Winamp()
    except:
        pass
    """
    title = w.getCurrentPlayingTitle()
    pos = w.getPlayingTrackPosition() / 1000
    pos_m, pos_s = divmod(pos, 60)
    len = w.getPlayingTrackLength()
    vol = int(w.getVolume() / 255.0 * 100)
    """
    cmd = ''

    if 'cmd' in request.GET:
        cmd = request.GET['cmd']

        if cmd == 'play':
            w.play()
        elif cmd == 'station':
            if 'param1' in request.GET:
                param1 = request.GET['param1']
                w.clearPlaylist()
                w.enqueueFile(param1)
            w.play()            
        elif cmd == 'stop':
            w.stop()
        elif cmd == 'pause':
            w.pause()
        elif cmd == 'next':
            w.next()
        elif cmd == 'prev':
            w.previous()
        elif cmd == 'open':
            if not is_open():
                open()
        elif cmd == 'close':
            close()
        elif cmd == 'volume':
            if 'param1' in request.GET:
                param1 = int(request.GET['param1'])
                vol = param1 / 100.0 * 255
                w.setVolume(vol)
    
    radia = radio.objects.all()        
    return render_to_response('index.html',
                              {'radia': radia},
                              context_instance=RequestContext(request))

    #return HttpResponse(html)
    #return HttpResponse(cmd+"<br>"+title+"<br>"+str(pos_m)+":"+str(pos_s)+"/"+str(len)+"<br>"+str(vol))