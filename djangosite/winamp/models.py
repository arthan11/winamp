from django.db import models

class radio_grupa(models.Model):
    nazwa = models.CharField(max_length=100)

class radio(models.Model):
    nazwa = models.CharField(max_length=100)
    grupa = models.ForeignKey(radio_grupa)
    adres = models.CharField(max_length=15)
    port = models.IntegerField()

